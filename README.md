## About
  This is an arch linux installer and configurer,based on the author machine and tastes.
  However, it is modular allowing to easily add new program and system configurations.
## Quick Usage
* Boot archiso, partition the disk(for example with cfdisk utility) and connect to the internet(for example plugging an cable)
* Still in the iso install 'git' and clone this repository
* Inside the repository edit the variables on 'config.sh' with your preferred values
* Still inside the root of the project, execute the 'install.sh' script
* When the machine shutsdown means the script is finished

## Adding modules
 Inside 'postInstall/configurers' there is the 'system' and 'userPrograms' folders, each one containing configuration modules, for example '/postInstall/configurers/userPrograms/neovim/'.
 
 A configuration module is just an directory with an 'install.sh' script inside, so to add modules, just create an directory inside those 'system' or 'userPrograms', and put any necessary files inside. Don't forget to create an script named 'install.sh' with any instructions that you like.
 
 Those 'install.sh' files will be executed as root, have access to all 'config.sh' variables, and the current directory will be the module folder.
 
 You can also change or delete the default modules since they are based on the author's taste.
