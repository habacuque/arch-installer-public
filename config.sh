## default values used during installation

# set export to all variables
set -a

## general configurations

# wich kernel to be used during instalation
# see https://wiki.archlinux.org/index.php/Kernel
KERNEL="linux-zen"
# default user to be created
USR="example"
USR_PASS="example"
ROOT_PASS="example"
# hostname of machine
HST="example"
# where the system is going to be installed
ROOT_PART="/dev/sda3"
SWAP_PART="/dev/sda2"
UEFI_PART="/dev/sda1"
# sometimes whe want to use an already present uefi partition
FORMAT_UEFI="true"

## post install configurations

# tty login messages
PRE_LOGIN_MSG=""
POST_LOGIN_MSG=""
TIMEZONE="/usr/share/zoneinfo/America/Sao_Paulo"
# from /etc/locale.gen
LOCALE="pt_BR.UTF-8 UTF-8"
AUTO_TTY1_LOGIN="true"
# from 'localectl set-x11-keymap' usage
X11_KEYBOARD='us "" altgr-intl'
# from 'localectl list-keymaps' list
TTY_KEYBOARD="us-acentos"
# check the archwiki for knowing the correct driver.Maybe you will have to make your own script 
# avalible scripts in './postInstall/gpuDriverInstallers'
GPU_SCRIPT="amdgpu.sh"

## which extra packages will be installed by pacman and yay
# the variables dont really matter , all of then will be directly passed to pacman or yay 
# but they help making a mental list of what is missing

DISPLAY_MANAGERS="ratpoison"
DISPLAY_MANAGERS_YAY=""

# daily use programs: terminal textEditor browser fonts player imageViwer printScreen nightMode clipboard
BASICS="rxvt-unicode neovim firefox ttf-ubuntu-font-family mpv feh scrot redshift xclip"
BASICS_YAY=""

PROGRAMMING="python-pip nodejs"
PROGRAMMING_YAY="jdk android-studio"

UTILITIES="htop neofetch unrar unzip"
UTILITIES_YAY=""

OTHERS="moc"
OTHERS_YAY=""
