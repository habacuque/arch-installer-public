## configures the installed system 
# it is called from the first install script and inherits it's variables

## debug mode , line by line execution and manually loading variables
#set -x
# trap read debug
#
# if [ $(id -u) -ne 0 ];then
#    echo "you are not root"
#    exit 1
#fi
# manually load configuration variables
#. ../config.sh


## navigate to this script's folder
cd $(dirname $0)

## pacman wont ask password for $USR
# makepkg(which is also used by yay) is executed as $USR
# and makepg uses pacman.We dont want to interrupt the script flow asking for password
echo "$USR ALL= NOPASSWD: /bin/pacman" >> /etc/sudoers

## enable multilib
# uncoments the [multilib] line and the next one
sed -i '/#\[multilib\]/ { s/^#// ; n ; s/^#// }' /etc/pacman.conf

## synchornize package databases and update system
pacman -Syu

## boot configuration
pacman --noconfirm -S grub efibootmgr os-prober
grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

## root password configuration
printf "${ROOT_PASS}\n${ROOT_PASS}" | passwd 

## new user configuration
useradd -m -g users -G wheel -s /bin/bash $USR
printf "${USR_PASS}\n${USR_PASS}" | passwd $USR 

## login messages
echo $PRE_LOGIN_MSG > /etc/issue
echo $POST_LOGIN_MSG > /etc/motd
touch /home/$USR/.hushlogin


## clock configuration
rm -f /etc/localtime
ln -sf $TIMEZONE /etc/localtime
hwclock --systohc


## locale configuration
# uncoments the specified locale from locale.gen file
sed -i "/#$LOCALE/ { s/^#// }" /etc/locale.gen
locale-gen
LANG=$(echo "$LOCALE" | cut -d" " -f1)
localectl set-locale LANG=$LANG


## yay installation
su - -l $USR -c 'git clone https://aur.archlinux.org/yay.git ; cd yay ; makepkg --noconfirm -si'
rm -rf /home/$USR/yay


## audio installation
pacman --noconfirm -S alsa pulseaudio

## graphics installation
pacman --noconfirm -S mesa lib32-mesa xorg-server xorg-xinit xf86-input-libinput $DISPLAY_MANAGERS
su - -l $USR -c "yay --noconfirm -S $DISPLAY_MANAGERS_YAY"
bash ./gpuDriverInstallers/$GPU_SCRIPT

## wine installation
pacman --noconfirm -S lib32-libpulse lib32-openal wine wine-mono
su - -l $USR -c 'yay --noconfirm -S dxvk-bin'
WINEPREFIX=/home/$USR/.wine setup_dxvk install

## extra packages installation
pacman --noconfirm -S $BASICS $PROGRAMMING $UTILITIES $OTHERS
su - -l $USR -c "yay --noconfirm -S $BASICS_YAY  $PROGRAMMING_YAY $UTILITIES_YAY $OTHERS_YAY"

## keyboard configuration
localectl set-keymap --no-convert $TTY_KEYBOARD
# the eval will prevent bash from adding single quotes to the double quotes of variable when executing command
eval "localectl set-x11-keymap --no-convert $X11_KEYBOARD"

## network installation
pacman --noconfirm -S networkmanager dhcpcd
echo $HST > /etc/hostname
printf "127.0.0.1\tlocalhost\n::1\tlocalhost\n127.0.1.1\t$HST\t$HST\n" > /etc/hosts
systemctl enable dhcpcd.service
systemctl enable NetworkManager.service

## automatic login configuration
if [ "$AUTO_TTY1_LOGIN" = "true" ];then
    mkdir -p /etc/systemd/system/getty\@tty1.service.d 
    printf "[service]\nExecStart=\nExecStart=-/usr/bin/agetty --autologin $USR --noclear %%I \$TERM" > /etc/systemd/system/getty\@tty1.service.d/override.conf
fi

## runs the system and user programs configurers
# changes to the folder and executes all install.sh scripts inside the 'configurers' directory
find $(pwd)/configurers -name "install.sh" -exec bash -c 'cd $(dirname {}) ;. {}' \;

## gives $USR permissions inside their folder
chown -R $USR:$USR /home/$USR
