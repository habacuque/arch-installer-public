alias ls='ls --color=auto'
alias grep='grep --colour=auto'
alias xclip='xclip -selection c'
PATH=$PATH:~/bin
PS1="\[\e[1;32m\] $( basename $(tty) ) \W \$ \[\e[0m\]"
