" vim-plug plug in manager
call plug#begin('~/.vim/plugged')

Plug 'https://github.com/trapd00r/neverland-vim-theme'

call plug#end()

" numero das linhas
set number

" search sem highlight
set nohlsearch

" sem status line
set laststatus=0

" deixa colorido
colorscheme neverland

" neverland tabs are ugly
hi TabLineFill cterm=NONE
hi TabLine cterm=NONE
hi TabLineSel cterm=NONE ctermbg=NONE
