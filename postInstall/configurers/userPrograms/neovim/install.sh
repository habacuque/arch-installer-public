mkdir -p /home/$USR/.config/nvim
/bin/cp ./init.vim /home/$USR/.config/nvim/
# install vim-plug
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
# install plugins
nvim -es '+:PlugInstall'
