# pacman -Sy ; pacman -S git ; git clone https://gitclone.com/habacuque/instalador.git
# this script installs arch linux and then calls the post install script
# assumes already partitioned disc and network connection

# debug mode , line by line execution
# set -x
# trap read debug

 if [ $(id -u) -ne 0 ];then
    echo "you are not root"
    exit 1
fi

# navigate to this script's folder
cd $(dirname $0)

# load configuration variables
. ./config.sh

# sets system clock
timedatectl set-ntp true

# formats system partition
mkfs.ext4 -F $ROOT_PART

# formats swap partition
mkswap -f $SWAP_PART
swapon $SWAP_PART

# formats uefi partition if specified
[ "$FORMAT_UEFI" = "true" ] && mkfs.fat -F32 $UEFI_PART

# mounts system partition
mount $ROOT_PART /mnt

# mounts uefi partition
mkdir -p /mnt/efi
mount $UEFI_PART /mnt/efi

# installs arch linux with basic utilities
pacstrap /mnt base $KERNEL linux-firmware base-devel util-linux

# auto partition mounting configuration
genfstab -U /mnt >> /mnt/etc/fstab

# changes to recently installed system
# and starts post installation script
chmod +x ./postInstall/install.sh
/bin/cp -r ../instalador /mnt/
arch-chroot /mnt /instalador/postInstall/install.sh

## removes instaler and shutsdown
rm -r /mnt/instalador
shutdown now
